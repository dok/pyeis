from __future__ import annotations

import logging
from collections.abc import Iterable
from types import SimpleNamespace
from typing import TYPE_CHECKING, Any, Literal, TypeAlias

if TYPE_CHECKING:
    import datetime

import requests
import zeep
import zeep.cache
import zeep.helpers
from lxml import etree  # lxml is already a dependency of zeep

ZeepObject: TypeAlias = zeep.xsd.valueobjects.CompoundValue

class EISKlient(zeep.Client):
    """Abstrakt EIS-klient."""

    def __init__(
        self,
        wsdl: str,
        identity: dict[str, str],
        gravitee_api_key: str,
        overskriv_endepunkt_url: bool = False,  # noqa: FBT001, FBT002 (boolean arguments)
        gravitee_debug: bool = False,  # noqa: FBT001, FBT002 (boolean arguments)
    ):
        self.identity = identity

        # Sjekk om passord ikke er oppgitt (inkludert bare en tom streng).
        if "Password" not in self.identity or not self.identity["Password"]:
            logging.warning("Du har ikke oppgitt noe passord.")

        session = requests.Session()
        session.headers["x-gravitee-api-key"] = gravitee_api_key
        if gravitee_debug:
            session.headers["x-debug"] = "on"
        transport = zeep.transports.Transport(  # type: ignore
            session=session,
            cache=zeep.cache.SqliteCache(),  # type: ignore
        )

        super().__init__(wsdl, transport=transport)  # type: ignore
        # Ved behov for korrigering av endepunkt-URL

        # Funksjon for serialisering
        self.serialize_object = zeep.helpers.serialize_object

        wsdl_address = self.service._binding_options["address"]  # noqa: SLF001 Private member accessed: `_binding_options`

        if wsdl_address.lower() != wsdl.split("?")[0].lower():
            logging.warning(
                "Endpoint address does not match\n{%s}",
                {"from_wsdl": wsdl_address, "wsdl_arg": wsdl},
            )

        if overskriv_endepunkt_url:
            wsdl_address = wsdl.split("?")[0]

    def _create_superfactory(self) -> SimpleNamespace:  # [str, Factory]
        """Create common type factory for all namespaces.

        Usage: fact.Sak -> <class 'zeep.xsd.dynamic_types.Sak'>
        """

        factories = {k: self.type_factory(k) for k in self.namespaces}

        # Map type names to namespaces
        # from dump(self) in src/zeep/wsdl/wsdl.py
        # with def create_prefixed_name(qname, schema) fra src/zeep/xsd/utils.py
        names_with_prefix: dict[str, str] = {}
        for i in sorted(self.wsdl.types.types, key=lambda k: k.qname or ""):
            # logging.debug(i.signature(schema=self.wsdl.types))
            try:
                prefixed_name = i.get_prefixed_name(schema=self.wsdl.types)
                pfn_spl = prefixed_name.split(":", maxsplit=1)
                if pfn_spl[1] in names_with_prefix:
                    logging.warning(
                        "Oh no! I have %s, but %s exists! Deleting both.",
                        i.get_prefixed_name(schema=self.wsdl.types),
                        names_with_prefix[pfn_spl[1]],
                    )
                    del names_with_prefix[pfn_spl[1]]
                names_with_prefix[pfn_spl[1]] = pfn_spl[0]
            except AttributeError as e:
                logging.debug(i)
                logging.warning(e)
        logging.debug("Got %s objects in dict.", len(names_with_prefix))

        return SimpleNamespace(
            **{k: factories[v][k] for k, v in names_with_prefix.items()}
        )

    def create_msg(self, operasjon: str, **kwargs) -> etree._Element:
        """Lag XML-dokument uten å sende det."""
        return self.create_message(self.service, operasjon, **kwargs)

    @staticmethod
    def pretty_print_msg(msg: etree._Element) -> str:
        """Formater XML-dokument."""
        # etree.tostring actually returns a bytes object
        return etree.tostring(
            msg, pretty_print=True, method="xml", encoding="unicode"
        )

    @staticmethod
    def felt_med_innhold(
        objekt: Iterable,
        related_format: Literal["med_tom_dict", "uten"] = "uten",
        resultat: list[dict[str, Any]] = [],
    ) -> list[dict[str, Any]]:
        """I en liste over objekt skriv ut alle felt med et innhold.

        # list[dict[str, dict[str, str | int | bool | datetime.datetime]]]:
        """
        if not resultat:
            resultat = []
        logging.info(type(objekt))
        if not (isinstance(objekt, (Iterable, str))):
            try:
                error = f"Objekttypen {type(object).__name__} støtter ikke iterasjon:\n {objekt}"
                raise TypeError(error)
            except TypeError as e:
                logging.exception(e)
                raise

        if not isinstance(objekt, list):
            # logging.debug("Lag liste av {}.".format(type(objekt).__name__))
            objekt = [objekt]
        for i in objekt:
            # if type(i).__module__ == "zeep.objects":  # If i is a zeep object
            type_name = type(i).__name__
            resultat.append({type_name: {}})
            for k in i:
                v = getattr(i, k)
                # logging.info(type(v))
                if isinstance(v, Iterable) and not isinstance(v, str):
                    # Tell antall verdier som ikke er None:
                    ant_none = sum(
                        [bool(getattr(v, k2) is not None) for k2 in v]
                    )
                    if ant_none == 0:
                        if related_format == "med_tom_dict":
                            resultat[-1][type_name][k] = {}
                        elif related_format == "uten":
                            pass
                        else:
                            raise ValueError(
                                "related_format må være 'med_tom_dict' eller 'uten'."
                            )

                    else:
                        # logging.info("{} har innhold.".format(k))
                        resultat_tmp = EISKlient.felt_med_innhold(v)
                        if len(resultat_tmp) == 1:
                            resultat[-1][type_name][k] = resultat_tmp.pop()
                        else:
                            resultat[-1][type_name][k] = resultat_tmp
                elif v is not None:
                    resultat[-1][type_name][k] = v
                else:
                    pass
        return resultat


class FunctionsServiceKlient(EISKlient):
    def __init__(
        self,
        wsdl: str,
        identity: dict[str, str],
        gravitee_api_key: str,
        overskriv_endepunkt_url: bool = False,  # noqa: FBT001, FBT002 (boolean arguments)
        gravitee_debug: bool = False,  # noqa: FBT001, FBT002 (boolean arguments)
    ):
        super().__init__(  # type: ignore
            # Too many positional arguments for "__init__" of "EISKlient"  [misc]
            wsdl,
            identity,
            gravitee_api_key,
            overskriv_endepunkt_url,
            gravitee_debug,
        )

        self.array_of_any_type = self.get_type(  # type: ignore
            "ns4:ArrayOfanyType"
        )
        self.any_object = zeep.xsd.AnyObject
        self.xsd_types = zeep.xsd

    def query_function_descriptors(self) -> ZeepObject:
        """Hent liste over tilgjengelige funksjoner."""
        return self.service.QueryFunctionDescriptors(  # type: ignore
            self.identity
        )

    def execute_function(
        self, name: str, parameters: list[zeep.xsd.AnyObject]
    ) -> Any:
        """Kall på en funksjon fra FunctionsService.

        Oppgi så mange parameter av typen zeep.xsd.AnyObject() i samme antall
        og rekkefølge som angitt i responsen for query_function_descriptors().
        Parameterne er uten navn.

        zeep.xsd.AnyObject(zeep.xsd.Int(), 1179304)
        zeep.xsd.AnyObject(zeep.xsd.String(), "kari.nordmann@example.com")
        """
        return self.service.ExecuteFunction(
            identity=self.identity,
            name=name,
            parameters=self.array_of_any_type(parameters),
        )

    def convert_to_pdf(
        self,
        dok_id: int,
        dok_ver: int,
        dok_var: str,
        jp_id: int,
        check_in_document: bool,  # noqa: FBT001
    ) -> None | ZeepObject:
        """Konverter dokument til arkivformat.

        … Til tross for beskrivelsen nedenfor.
        {
            "Description": "Kopierer avsmot til sakspart",
            "Name": "ConvertToPdf",
            "Parameters": {
                "Parameter": [
                    {
                        "Name": "documentDescriptionId",
                        "Type": "Int32"
                    },
                    {
                        "Name": "version",
                        "Type": "Int32"
                    },
                    {
                        "Name": "variant",
                        "Type": "String"
                    },
                    {
                        "Name": "linkToRegistryEntry",
                        "Type": "Int32"
                    },
                    {
                        "Name": "checkInDocument",
                        "Type": "Boolean"
                    }
                ]
            }
        }
        """

        return self.execute_function(
            name="ConvertToPdf",
            parameters=[
                self.any_object(
                    self.xsd_types.Int(), dok_id
                ),  # documentDescriptionId
                self.any_object(
                    self.xsd_types.Int(), dok_ver
                ),  # versionNumber
                self.any_object(
                    self.xsd_types.String(), dok_var
                ),  # variantFormatId
                self.any_object(
                    self.xsd_types.Int(), jp_id
                ),  # linkToRegistryEntry
                self.any_object(
                    self.xsd_types.Boolean(), check_in_document
                ),  # checkInDocument
            ],
        )


class ObjectModelServiceKlient(EISKlient):
    """
    Delete: Deletes the dataObject.
    DeleteById: Deletes the DataObject identified by its primaryKeys
    Fetch: Queries the specified arguments.
    FilteredQuery: Executes a filtered query.
    GetCustomFieldDescriptors:
    Insert: Inserts the dataObjects.
    PredefinedQuery: Executes a query predefined by the system.
    Submit:
    Update: Updates the dataObjects.
    """

    def __init__(
        self,
        wsdl: str,
        identity: dict[str, str],
        gravitee_api_key: str,
        overskriv_endepunkt_url: bool = False,  # noqa: FBT001, FBT002 (boolean arguments)
        gravitee_debug: bool = False,  # noqa: FBT001, FBT002 (boolean arguments)
    ):
        super().__init__(  # type: ignore
            # Too many positional arguments for "__init__" of "EISKlient"  [misc]
            wsdl,
            identity,
            gravitee_api_key,
            overskriv_endepunkt_url,
            gravitee_debug,
        )

        self.t_fact = self._create_superfactory()

        self.fq_args = self.t_fact.FilteredQueryArguments  # type: ignore
        self.array_of_string = self.t_fact.ArrayOfstring  # type: ignore
        self.array_of_data_object = self.t_fact.ArrayOfDataObject  # type: ignore
        self.id_ = self.t_fact.ID  # type: ignore
        self.ref = self.t_fact.IDREF  # type: ignore

    def delete(self, data_object: ZeepObject) -> None:
        self.service.Delete(identity=self.identity, dataObject=data_object)

    def filtered_query(self, args: ZeepObject) -> ZeepObject:
        return self.service.FilteredQuery(  # type: ignore
            identity=self.identity, arguments=args
        )

    def fq(
        self,
        data_object_name: str,
        filter_expression: str | None = None,
        related_objects: list[str | None] | None = None,
        return_total_count: bool | None = None,
        skip_count: int | None = None,
        sort_expression: str | None = None,
        take_count: int | None = None,
    ) -> ZeepObject:
        """Utfør FilteredQuery.

        DataObjectName: Navn på objekttypen søket gjelder for. For eksempel
            Sak. FilterExpression: Angir kriterie(r) for søket
        RelatedObjects: Relaterte objekter som ønskes returnert sammen med
            hovedobjekttypen. ReturnTotalCount: Angir om operasjonen skal
            returnere totalt antall poster som tilfredsstiller kriteriene.
        SkipCount: Antall poster som skal forkastes for resultatet returneres.
            Denne kan brukes sammen med TakeCount for å utføre paginering.
        SortExpression: Angir sorteringsnøkkel for resultatet. For eksempel "Id
            ASC". ASC benyttes for stigende og DESC for synkende sortering.
        TakeCount: Maksimalt antall poster som ønskes returnert. Denne kan
            brukes sammen med SkipCount for å utføre paginering.

        # TODO: use filtered_query_pagination
        """
        return self.filtered_query(
            self.fq_args(
                DataObjectName=data_object_name,
                FilterExpression=filter_expression,
                RelatedObjects=self.array_of_string(related_objects),
                ReturnTotalCount=return_total_count,
                SkipCount=skip_count,
                SortExpression=sort_expression,
                TakeCount=take_count,
            )
        )

    def get_custom_field_descriptors(
        self, data_object_name: str, category: str, id_: int
    ) -> ZeepObject:
        """«Gets the custom field descriptors for a DataObject.» ???"""
        return self.service.GetCustomFieldDescriptors(  # type: ignore
            identity=self.identity,
            dataObjectName=data_object_name,
            category=category,
            id=id_,
        )

    def initialize(
        self,
        data_object: ZeepObject,
        *,
        return_access_rights: bool,
        return_required_fields: bool,
    ) -> ZeepObject:
        """Apply the default values to the target.

        Fra dokumentasjonen:

        I spesielle tilfeller ønsker man å populere et nytt objekt med verdier
        uten at objektet blir lagret. Et slikt tilfelle kan være et
        redigeringsskjermbilde for oppretting av et nytt objekt. I et slikt
        redigeringsskjermbilde ønsker man gjerne å presentere verdiene som
        *kommer* til å bli lagret. Metoden *Initialize* eksponerer denne
        funksjonaliteten.

        Verdiene som blir satt på et objekt ved å kjøre *Initialize* vil
        variere med egenskapene til brukeren (for eksempel rolle) som er
        logget inn samt konfigurerte standardverdier i Elements.  NB: Det gir
        ingen mening å kjøre *Initialize* for objekter som allerede er lagret
        i Elements, da de objektene allerede har fått alle standardverdier
        utfylt.
        """
        return self.service.Initialize(  # type: ignore
            identity=self.identity,
            arguments=self.get_type("ns1:InitializeArguments")(  # type: ignore
                DataObject=data_object,
                ReturnAccessRights=return_access_rights,
                ReturnRequiredFields=return_required_fields,
            ),
        )

    def insert(self, data_objects: ZeepObject) -> ZeepObject:
        return self.service.Insert(  # type: ignore
            identity=self.identity, dataObjects=data_objects
        )

    def ins(self, array_of_data_object: list[ZeepObject]) -> ZeepObject:
        return self.insert(self.array_of_data_object(array_of_data_object))

    def update(self, data_objects: ZeepObject) -> ZeepObject:
        """Oppdater eksisterende objekt.

        Feilmeldingen «TypeError: argument of type 'int' is not iterable» kan
        bety at du refererer til et beslektet objekt, ikke til en id (for
        eksempel (Sak i stedet for SakId).
        """
        return self.service.Update(  # type: ignore
            identity=self.identity, dataObjects=data_objects
        )

    def upd(self, array_of_data_object: list[ZeepObject]) -> ZeepObject:
        return self.update(self.array_of_data_object(array_of_data_object))

    def fetch(
        self,
        data_object_name: str,
        primary_keys: dict[str, Any],
        related_objects: list[str] = [],
        return_access_rights: bool = False,
        return_required_fields: bool = False,
    ) -> ZeepObject:
        """Hent ett spesifikt objekt fra Elements.

        DataObjectName: Hva slags type objekt som skal hentes.
        PrimaryKeys: Angir primærnøklene til objektet som skal hentes. [Det vil
            si attributtene som gjør at man kan identifisere akkurat dette
            objektet.]
        RelatedObjects: Relaterte objekter som ønskes returnert sammen med
            hovedobjekttypen.
        ReturnAccessRights: Returnere et objekt som beskriver den aktive
            brukerens tilgang til objektet.
        ReturnRequiredFields: Returner et objekt som beskriver hvilke felt som
            er påkrevd. [Synes å bety hvilke verdier som må være satt for
            objektet automatisk eller manuelt, ikke hvilke verdier som må
            settes eksplisitt ved opprettelse.]

        svar:
            FetchResult
                AccessRights
                DataObject
                ObjectRights
                RequiredFields

        """
        primary_keys_formatted = [
            {"Key": k, "Value": v} for k, v in primary_keys.items()
        ]
        return self.service.Fetch(
            identity=self.identity,
            arguments=self.t_fact.FetchArguments(
                DataObjectName=data_object_name,
                PrimaryKeys=self.t_fact.PrimaryKeyCollection(
                    PrimaryKey=primary_keys_formatted
                ),
                RelatedObjects=related_objects,
                ReturnAccessRights=return_access_rights,
                ReturnRequiredFields=return_required_fields,
            ),
        )

    @staticmethod
    def qr_til_liste(query_result: ZeepObject) -> list[ZeepObject]:
        """Hent listen over DataObjects ut av EIS QueryResult.

        klasse: zeep.objects.QueryResult
        """
        try:
            data_object = query_result["DataObjects"]["DataObject"]
            # NB: TotalCount angir alle objekt som kan hentes, ikke bare
            # antallet i responsen.
            logging.debug("Antall objekt i responsen: %s", len(data_object))
        except TypeError:
            msg = "Fikk ingen treff. Avbryter"
            raise TypeError(msg) from TypeError
        return data_object  # type: ignore

    def filtered_query_bare_data_object(
        self, args: ZeepObject
    ) -> list[ZeepObject]:
        """Hent listen over DataObjects ut av EIS QueryResult."""
        return self.qr_til_liste(
            self.service.FilteredQuery(identity=self.identity, arguments=args)
        )

    def filtered_query_pagination(self, args: ZeepObject) -> ZeepObject:
        """Del opp FilteredQuery i spørringer etter `take_count` resultat."""
        if args.get("TakeCount"):
            if not args["SkipCount"]:
                args["SkipCount"] = 0
            foerste_runde = True
            while True:
                logging.debug(
                    "FilteredQuery: args['SkipCount']=%s, args['TakeCount']=%s",
                    args["SkipCount"],
                    args["TakeCount"],
                )
                fq: ZeepObject = self.filtered_query(args)
                data_object = self.qr_til_liste(fq)
                if foerste_runde:
                    objekter = fq
                else:
                    objekter["DataObjects"]["DataObject"].extend(data_object)
                logging.debug(
                    "Mottok %s objekter. Totalt: %s",
                    len(data_object),
                    len(objekter["DataObjects"]["DataObject"]),
                )
                if len(data_object) < args["TakeCount"]:
                    break

                args["SkipCount"] += args["TakeCount"]
                # Dersom det er angitt et maks antall …
                if args["ReturnTotalCount"]:
                    mulig_ant = args["SkipCount"] + args["TakeCount"]
                    maks_ant = args["ReturnTotalCount"]
                    logging.debug(
                        "mulig_ant=%s, maks_ant=%s", mulig_ant, maks_ant
                    )
                    # Dersom mulig TotalCount > ReturnTotalCount
                    if mulig_ant > maks_ant:
                        # Reduser TakeCount med differansen
                        args["TakeCount"] -= mulig_ant - maks_ant
                        logging.debug(
                            'Ok, da nøyer vi oss med args["TakeCount"]=%s',
                            args["TakeCount"],
                        )
                        # Hvis differansen er null, slik at TakeCount blir 0,
                        # stopp.
                        if args["TakeCount"] == 0:
                            break
                foerste_runde = False
            return objekter
        else:  # noqa: RET505 Unnecessary `else` after `return` statement
            return self.filtered_query(args)

    @staticmethod
    def info_om_objekt(obj: ZeepObject) -> dict[str, str | int]:
        """Hent ut opplysninger om et objekt fra QueryResult."""
        oppl = {}
        oppl["obj_navn"] = str(type(obj)).split("zeep.objects.")[1][:-2]
        if oppl["obj_navn"] == "Dokumentversjon":
            oppl["id"] = obj["DokumentbeskrivelseId"]
            oppl["dokversjon"] = obj["Versjonsnummer"]
            oppl["dokvariant"] = obj["VariantId"]
            oppl["dokfilref"] = obj["Dokumentreferanse"]
            if obj["UtsjekketAvId"] != 0:
                oppl["utsj_kommentar"] = "Filen er ikke innsjekket."
        else:
            oppl["id"] = obj["Id"]
            if oppl["obj_navn"][-5:] == "Lenke":
                try:
                    oppl["jp_id"] = obj["JournalpostId"]
                except KeyError:
                    oppl["sa_id"] = obj["SakId"]
                oppl["lenketype_id"] = obj["LenketypeId"]
                oppl["lenke_url"] = obj["LenkeUrl"]
        return oppl  # type: ignore

    @staticmethod
    def lag_eph_lenke(
        handling: str,
        base_url: str,
        id_: int,
        database: str | None = None,
        ve_versjon: int | None = None,
        ve_variant: str | None = None,
        ve_dokformat: str | None = None,
        ve_status: str | None = None,
    ) -> str:
        """Lag en lenke for å utføre en handling i ephorte.

        Støtter følgende handling: show_jp, get_doc, avskriv_direkte
        base_url slutter på 'ephorte/' (eller tilsvarende).
        """
        url = ""
        if handling == "show_jp":
            url = (
                f"{base_url}locator/?SHOW=JP&JP_ID={id_}"
                f"&database={database}"
            )
        elif handling == "get_doc":
            for i in [
                database,
                ve_versjon,
                ve_variant,
                ve_dokformat,
                ve_status,
            ]:
                if not i:
                    logging.error(f"Kan ikke lage lenke til fil uten {i}")  # noqa
            url = (
                f"{base_url}shared/aspx/GetDoc.aspx?VE_DOKID_DB={id_}"
                f"&VE_VERSJON={ve_versjon}&VE_VARIANT_VF={ve_variant}"
                f"&VE_DOKFORMAT_LF={ve_dokformat}&VE_STATUS_XX={ve_status}"
                f"&EphorteDb={database}&database={database}"
            )
        elif handling.lower() == "avskriv_direkte":
            url = (
                base_url + "shared/aspx/default/FunctionHandler.aspx?"
                "f=AvskrivDirekteJP&JP_ID=" + str(id_)
            )
        else:
            logging.error(f'Ugyldig handling "{handling}". Avslutter.')  # noqa
        return url

    @staticmethod
    def lag_elt_lenke(
        handling: str, base_url: str, id_: int, database: str
    ) -> str:
        """Lag en lenke for å utføre en handling i Elements

        Støtter følgende objekt: vis_jp
        base_url slutter på 'Elements/' (eller tilsvarende).
        """
        url = ""
        if handling == "vis_jp":
            url = (
                f"{base_url}rm/{database}/#nav=/locator/registryEntries/"
                f"{id_}"
            )
        else:
            logging.error('Ugyldig handling "%s". Avslutter.', handling)
        return url


class DocumentServiceKlient(EISKlient):
    def get_doc_content_base(
        self, dok_id: int, variant: str = "P", versjon: int = 1
    ) -> ZeepObject:
        """Hent dokumentfil ved å angi dok.-ID og evt. variant og versjon."""
        return self.service.GetDocumentContentBase(  # type: ignore
            _soapheaders={
                "Identity": self.identity,
                "DocumentId": dok_id,
                "Variant": variant,
                "Version": versjon,
            }
        )

    def upload_file(
        self,
        content: bytes,
        file_name: str,
        content_type: str | None = None,
        storage_identifier: str | None = "ObjectModelService",
    ) -> ZeepObject:
        """Last opp en fil til et midlertidig lagringsområde.

        Typisk respons med standard storage_identifier:

        ```
        {
            "header": {
                "FileName": "UPLOAD_{ObjectModelService}_44c41bf2-d409-4fcc-b6c2-2e0790ce8665",
                "Identifier": "ObjectModelService",
            },
            "body": None,
        }
        ```

        `FileName` kan deretter brukes i feltet
        OMS.Dokversjon.Dokumentreferanse
        """
        if not isinstance(content, bytes):
            msg = f"Fildata er {type(content)}, må være {bytes}."
            raise TypeError(msg)
        return self.service.UploadFile(  # type: ignore
            _soapheaders={
                "ContentType": content_type,
                "EphorteIdentity": self.identity,
                "FileName": file_name,
                "StorageIdentifier": storage_identifier,
            },
            Content=content,
        )

    def checkout(self, dok_id: int, variant: str, versjon: int) -> ZeepObject:
        """Sjekk ut dokument."""
        return self.service.Checkout(  # type: ignore
            _soapheaders={
                "DocumentId": dok_id,
                "Identity": self.identity,
                "Variant": variant,
                "Version": versjon,
            }
        )

    def cancel_checkout_doc(
        self, dok_id: int, variant: str, versjon: int
    ) -> ZeepObject:
        """Angre utsjekking av dokument."""
        return self.service.CancelCheckout(  # type: ignore
            _soapheaders={
                "DocumentId": dok_id,
                "Identity": self.identity,
                "Variant": variant,
                "Version": versjon,
            }
        )

    def checkin(
        self,
        content: bytes,
        dok_id: int,
        variant: str,
        versjon: int,
        content_type: str | None = None,
    ) -> ZeepObject:
        """Last opp og sjekk inn dokument."""
        return self.service.Checkin(  # type: ignore
            _soapheaders={
                "ContentType": content_type,
                "DocumentCriteria": self.get_type(  # type: ignore
                    "ns1:DocumentCriteria"
                )(
                    DocumentId=dok_id,
                    EphorteIdentity=self.identity,
                    Variant=variant,
                    Version=versjon,
                ),
            },
            Content=content,
        )

    def replace(
        self,
        content: bytes,
        dok_id: int,
        variant: str,
        versjon: int,
        content_type: str | None = None,
    ) -> ZeepObject | None:
        """Erstatt et dokument (NB: filnavn med filtype forblir likt)."""
        self.checkout(dok_id=dok_id, variant=variant, versjon=versjon)
        try:
            return self.checkin(
                content=content,
                dok_id=dok_id,
                variant=variant,
                versjon=versjon,
                content_type=content_type,
            )
        except zeep.exceptions.Fault as e:
            logging.exception(msg="Kunne ikke sjekke inn dokument", exc_info=e)
            logging.info("Angrer utsjekking.")
            return self.cancel_checkout_doc(
                dok_id=dok_id, variant=variant, versjon=versjon
            )


class ChangeTrackingServiceKlient(EISKlient):
    def get_changes_for_cases(
        self, case_ids: list[int], after: datetime.datetime | None = None
    ) -> ZeepObject:
        """->GetChangesForCaseDataObjectsResult: ns1:ArrayOfDataObjectChange"""
        return self.service.GetChangesForCaseDataObjects(  # type: ignore
            identity=self.identity,
            caseIds=self.get_type("ns2:ArrayOfint")(case_ids),  # type: ignore
            after=after,
        )

    def get_changes_for_records(
        self, record_ids: list[int], after: datetime.datetime | None = None
    ) -> ZeepObject:
        """->GetChangesForRecordDataObjectsResult: ns1:ArrayOfDataObjectChg…"""
        return self.service.GetChangesForRecordDataObjects(  # type: ignore
            identity=self.identity,
            recordIds=self.get_type("ns2:ArrayOfint")(  # type: ignore
                record_ids
            ),
            after=after,
        )
