import datetime
import json
import logging
from typing import TypeAlias

import dynaconf  # type: ignore
import pyeis  # type: ignore
import pytest

ZeepObject: TypeAlias = pyeis.ZeepObject


@pytest.fixture(scope="module")
def config() -> dynaconf.base.LazySettings:
    return dynaconf.Dynaconf(
        settings_files=["config.toml", ".secrets.toml"],
        environments=True,
        merge_enabled=True,
    )


class TestEISKlient:
    @pytest.fixture(scope="class")
    def eis(self, config: dynaconf.base.LazySettings) -> pyeis.EISKlient:
        return pyeis.EISKlient(
            wsdl=config.elt.oms_url,
            gravitee_api_key=config.elt.api_noekkel,
            identity={
                "Database": config.elt.db,
                "UserName": config.elt.username,
                "Password": config.elt.password,
                "ExternalSystemName": config.elt.ext_system_name,
            },
        )

    @pytest.mark.parametrize(
        "identity",
        (
            {
                "Database": "db",
                "UserName": "username",
                "Password": "",
                "ExternalSystemName": "system",
            },
            {
                "Database": "db",
                "UserName": "username",
                "ExternalSystemName": "system",
            },
            pytest.param(
                {
                    "Database": "db",
                    "UserName": "username",
                    "Password": "password",
                    "ExternalSystemName": "system",
                },
                marks=[pytest.mark.xfail],
            ),
        ),
    )
    def test_passordsjekk(
        self,
        config: dynaconf.base.LazySettings,
        caplog: pytest.LogCaptureFixture,
        identity: dict[str, str],
    ) -> None:
        pyeis.EISKlient(
            wsdl=config.elt.oms_url,
            gravitee_api_key=config.elt.api_noekkel,
            identity=identity,
        )
        assert "Du har ikke oppgitt noe passord" in caplog.text


class TestObjectModelServiceKlient:
    @pytest.fixture(scope="class")
    def oms_kli(
        self, config: dynaconf.base.LazySettings
    ) -> pyeis.ObjectModelServiceKlient:
        return pyeis.ObjectModelServiceKlient(
            wsdl=config.elt.oms_url,
            gravitee_api_key=config.elt.api_noekkel,
            identity={
                "Database": config.elt.db,
                "UserName": config.elt.username,
                "Password": config.elt.password,
                "ExternalSystemName": config.elt.ext_system_name,
            },
            overskriv_endepunkt_url=True,
        )

    @pytest.fixture(scope="class")
    def ds_kli(
        self, config: dynaconf.base.LazySettings
    ) -> pyeis.DocumentServiceKlient:
        return pyeis.DocumentServiceKlient(
            wsdl=config.elt.ds_url,
            gravitee_api_key=config.elt.api_noekkel,
            identity={
                "Database": config.elt.db,
                "UserName": config.elt.username,
                "Password": config.elt.password,
                "ExternalSystemName": config.elt.ext_system_name,
            },
            overskriv_endepunkt_url=True,
        )

    @pytest.fixture(scope="class")
    def klasser(
        self, oms_kli: pyeis.ObjectModelServiceKlient
    ) -> dict[str, ZeepObject]:
        return {
            "sa": oms_kli.get_type("ns4:Sak"),
            "jp": oms_kli.get_type("ns4:Journalpost"),
            "am": oms_kli.get_type("ns4:AvsenderMottaker"),
            "dl": oms_kli.get_type("ns4:Dokumentreferanse"),
            "db": oms_kli.get_type("ns4:Dokumentbeskrivelse"),
            "dv": oms_kli.get_type("ns4:Dokumentversjon"),
        }

    @pytest.fixture(scope="class")
    def sak_med_mer(
        self,
        oms_kli: pyeis.ObjectModelServiceKlient,
        ds_kli: pyeis.DocumentServiceKlient,
        klasser: dict[str, ZeepObject],
    ) -> ZeepObject:
        """Opprett en sak underliggende objekter."""
        # Last opp en fil og hent filreferanse.
        import io

        filinnhold = io.BytesIO(b"Dette er bare en test.")
        filnavn = "testfil.txt"
        respons = ds_kli.upload_file(filinnhold.read(), filnavn)
        filref = respons["header"]["FileName"]

        return oms_kli.insert(
            oms_kli.array_of_data_object(
                [
                    klasser["sa"](Tittel="Testsak", attr__Id="sa"),
                    klasser["jp"](
                        Innholdsbeskrivelse="Test",
                        DokumenttypeId="U",
                        JournalstatusId="R",
                        Sak=klasser["sa"](Ref="sa"),
                        # SakId=252339,
                        attr__Id="jp",
                    ),
                    klasser["am"](
                        Innholdstype=True,  # dvs. mottaker
                        Journalpost=klasser["jp"](Ref="jp"),
                        Navn="Ekstern mottaker",
                        Person=True,
                        # UntattOffentlighet=True,
                    ),
                    klasser["am"](
                        AdministrativEnhetId=99,
                        Innholdstype=True,  # dvs. mottaker
                        JournalEnhetId="DS01",
                        Journalpost=klasser["jp"](Ref="jp"),
                        KopimottakerMedavsender=True,
                        # SaksbehandlerId=187,
                        UntattOffentlighet=False,
                    ),
                    klasser["db"](
                        attr__Id="db",
                    ),
                    klasser["dv"](
                        Dokumentbeskrivelse=klasser["db"](Ref="db"),
                        Dokumentreferanse=filref,
                        LagringsformatId="TXT",
                    ),
                    klasser["dl"](
                        Dokumentbeskrivelse=klasser["db"](Ref="db"),
                        Journalpost=klasser["jp"](Ref="jp"),
                        TilknytningskodeId="H",  # Nødvendig
                    ),
                    klasser["jp"](
                        Innholdsbeskrivelse="Test av avskrivning",
                        DokumenttypeId="I",
                        JournalstatusId="S",
                        Sak=klasser["sa"](Ref="sa"),
                        attr__Id="jp_avskr",
                        SaksbehandlerId=19686,  # PEGA EIS
                        AdministrativEnhetId=103,  # HRA
                    ),
                    klasser["am"](
                        Innholdstype=False,  # dvs. avsender
                        Journalpost=klasser["jp"](Ref="jp_avskr"),
                        Navn="Ekstern avsender",
                        Person=True,
                    ),
                ]
            )
        )

    def test_filtered_query(
        self, oms_kli: pyeis.ObjectModelServiceKlient
    ) -> None:
        jp = oms_kli.qr_til_liste(oms_kli.fq("Journalpost", "Id=1"))[0]
        jp = oms_kli.felt_med_innhold([jp])[0]
        assert jp == {
            "Journalpost": {
                "AdministrativEnhetId": 103,
                "AntallVedlegg": 0,
                "ArkiveresPaaPapir": False,
                "AvsenderMottakerUntattOffentlighet": False,
                "Dokumentdato": datetime.datetime(2024, 2, 28, 0, 0),
                "Dokumentnummer": 1,
                "DokumenttypeId": "X",
                "Endret": datetime.datetime(2024, 2, 28, 20, 27, 8),
                "HarRestanse": "Ingen",
                "Hoveddokumenttype": "DOCX",
                "Id": 1,
                "Innholdsbeskrivelse": "Test 2024!",
                "InnholdsbeskrivelseOffentlig": "Test 2024!",
                "InnholdsbeskrivelsePersonnavn": "Test 2024!",
                "JournalEnhetId": "DS01",
                "Journalaar": 2024,
                "Journaldato": datetime.datetime(2024, 2, 28, 0, 0),
                "JournalstatusId": "R",
                "Lest": False,
                "Opprettet": datetime.datetime(2024, 2, 28, 20, 26, 54),
                "OpprettetAvId": 18564,
                "Publisert": False,
                "SakId": 1,
                "SaksbehandlerId": 0,
                "Sekvensnummer": 1,
                "UdatertDokument": False,
                "UnntattOffentlighet": False,
                "UtlaantTilId": 0,
                "JournalstatusHovedId": 7,
                "attr__Id": "i1",
            }
        }

    def test_superfactory(
        self, oms_kli: pyeis.ObjectModelServiceKlient
    ) -> None:
        dokref_1 = oms_kli.t_fact.Dokumentreferanse(Id=1)
        felter_med_innhold = oms_kli.felt_med_innhold([dokref_1])
        assert felter_med_innhold

    def test_sak(
        self, oms_kli: pyeis.ObjectModelServiceKlient, sak_med_mer: ZeepObject
    ) -> None:
        kompakt = oms_kli.felt_med_innhold(sak_med_mer)
        logging.warning(json.dumps(kompakt, indent=2, default=str))

        jp_id = next(
            i for i in sak_med_mer if type(i).__name__ == "Journalpost"
        )["Id"]

        jp = oms_kli.qr_til_liste(oms_kli.fq("Journalpost", f"Id={jp_id}"))[0]
        logging.warning(
            json.dumps(oms_kli.felt_med_innhold([jp]), indent=2, default=str)
        )

        for i in range(5):
            jstatus = jp["JournalstatusId"]
            logging.warning(f"{jstatus}")
            logging.warning("Endrer journalstatus")
            if jstatus == "F":
                jp["JournalstatusId"] = "R"
            elif jstatus == "R":
                jp["JournalstatusId"] = "F"
            else:
                logging.error("Eller vent … Hva skjer her?")
                break
            respons = oms_kli.upd([jp])
            jp = next(i for i in respons if type(i).__name__ == "Journalpost")

        # logging.warning(json.dumps(sak_kompakt, indent=2, default=str))
        # breakpoint()
        # logging.warning(od)
        # logging.warning(sak_med_mer)

    @pytest.fixture
    def fetch_response(self, oms_kli):
        primary_keys = {"Saksaar": 2025, "Sekvensnummer": 1}
        res = oms_kli.fetch(data_object_name="Sak", primary_keys=primary_keys)
        # logging.info(type(res))  # <class 'zeep.objects.FetchResult'>
        # logging.info(type(res["DataObject"]))  # <class 'zeep.objects.Sak'>
        # logging.info(type(res.DataObject))  # <class 'zeep.objects.Sak'>
        # logging.info(type(res).__bases__)  # (<class 'zeep.xsd.valueobjects.CompoundValue'>,)
        # logging.info(type(res).__mro__)  # (<class 'zeep.objects.FetchResult'>, <class 'zeep.xsd.valueobjects.CompoundValue'>, <class 'object'>)
        return res

    def test_fetch(self, fetch_response):
        # logging.info(fetch_response)
        assert dir(fetch_response) == [
            "AccessRights",
            "DataObject",
            "ObjectRights",
            "RequiredFields",
        ]

    def felt_med_innhold(self, fetch_response):
        expected = [
            {
                "FetchResult": {
                    "AccessRights": {},
                    "DataObject": {
                        "Sak": {
                            "AnsvarligEnhet": {},
                            "AnsvarligPerson": {},
                            "AntallJournalposter": 3,
                            "Arkivdel": {},
                            "ArkivdelId": "SAKSARKIV",
                            "ArkiveresPaaPapir": False,
                            "Endret": datetime.datetime(
                                2025, 1, 15, 12, 39, 9
                            ),
                            "FraArkivdel": {},
                            "Id": 653,
                            "JournalEnhet": {},
                            "JournalEnhetId": "DS01",
                            "Kassasjonskode": {},
                            "Lest": False,
                            "Mappetype": {},
                            "Opprettet": datetime.datetime(
                                2025, 1, 8, 9, 16, 8
                            ),
                            "OpprettetAvId": 5,
                            "Primaerklassering": {},
                            "Prosjekt": {},
                            "Publisert": True,
                            "Saksaar": 2025,
                            "SaksansvarligEnhetId": 2,
                            "SaksansvarligPersonId": 5,
                            "Saksdato": datetime.datetime(2025, 1, 8, 0, 0),
                            "Saksstatus": {},
                            "SaksstatusId": "R",
                            "Sekundaerklassering": {},
                            "Sekvensnummer": 1,
                            "SisteJournaldato": datetime.datetime(
                                2025, 1, 15, 0, 0
                            ),
                            "Tilgangsgruppe": {},
                            "Tilgangskode": {},
                            "Tittel": "Forhåndstest oppretting av sak",
                            "TittelOffentlig": "Forhåndstest oppretting av sak",
                            "TittelPersonnavn": "Forhåndstest oppretting av sak",
                            "UntattOffentlighet": False,
                            "UtlaantTil": {},
                            "UtlaantTilId": 0,
                            "SakFar": {},
                            "attr__Id": "i1",
                        }
                    },
                    "ObjectRights": {},
                    "RequiredFields": {},
                }
            }
        ]
        assert oms_kli.felt_med_innhold(fetch_response) == expected

    def test_create_msg(self, oms_kli):
        identity = {
            "Database": "",
            "UserName": "",
            "Password": "",
            "ExternalSystemName": "",
        }
        data_object = [
            oms_kli.t_fact.Sak(Id=1),
            oms_kli.t_fact.Journalpost(Id=2),
        ]
        msg = oms_kli.create_msg(
            "Insert",
            identity=identity,
            dataObjects=oms_kli.t_fact.ArrayOfDataObject(data_object),
        )
        # logging.info(oms_kli.pretty_print_msg(msg))
        assert type(msg).__name__ == "_Element"

    """
    def TODO_move_to_pyeis(oms):
        sak_med_tittel = oms.t_fact.Sak(Tittel="test")
        pp(oms.felt_med_innhold(sak_med_tittel))

        fin_sak = oms.t_fact.Sak(**{"Tittel": "test", "Id": 3})
        try:
            stygg_sak = oms.t_fact.Sak(**{"Tittel": "test", "baretull": 3})
        except TypeError as e:
            logging.error(str(e)[:100] + " …")
        stygg_sak = oms.t_fact.Sak(**{"Tittel": "test", "Id": 3})

        # Does not get validated
        stygg_sak.__setattr__("baretull", 3)
        # logging.debug(oms.felt_med_innhold(stygg_sak))

        # Extra value is ignored, no error
        for i in [fin_sak, stygg_sak]:
            # logging.debug(oms.felt_med_innhold(i))
            msg = oms.create_message(
                oms.service,
                "Insert",
                identity=oms.identity,
                dataObjects=oms.t_fact.ArrayOfDataObject([i]),
            )
            # logging.debug(pretty_print_msg(msg))

        # Just doesn't work
        # TypeError: {http://www.gecko.no/ephorte/services/objectmodel/v3/no}ArrayOfDataObject() got an unexpected keyword argument 'Sak'. Signature: `DataObject: {http://www.gecko.no/ephorte/services/objectmodel/v3/no}DataObject[]`
        # pretty_print_msg(
        #     oms.create_message(
        #         oms.service,
        #         "Insert",
        #         identity=oms.identity,
        #         dataObjects=[{"Sak": {"Tittel": m.tittel}}],
        #     )
        # )
    """

class TestDocumentServiceKlient:
    @pytest.fixture(scope="class")
    def ds_kli(
        self, config: dynaconf.base.LazySettings
    ) -> pyeis.DocumentServiceKlient:
        return pyeis.DocumentServiceKlient(
            wsdl=config.elt.ds_url,
            gravitee_api_key=config.elt.api_noekkel,
            identity={
                "Database": config.elt.db,
                "UserName": config.elt.username,
                "Password": config.elt.password,
                "ExternalSystemName": config.elt.ext_system_name,
            },
            overskriv_endepunkt_url=True,
        )

    filinnhold = b"\x00\x00\x00\x00\xff\xff\xff\xff"
    filnavn = "test.bin"
    dok_id = 469

    # dok_id = 1695869  # 00001111-testfil

    # def test_last_opp_fil(self):
    #     respons = self.eis.upload_file(self.filinnhold,
    #                                    file_name=self.filnavn)
    #     logging.debug(respons)
    #     assert respons

    def test_erstatt_fil(self, ds_kli: pyeis.DocumentServiceKlient) -> None:
        respons = ds_kli.replace(
            content=self.filinnhold, dok_id=self.dok_id, versjon=1, variant="P"
        )
        logging.debug(respons)
        assert not respons

    def test_last_ned_fil(self, ds_kli: pyeis.DocumentServiceKlient) -> None:
        respons = ds_kli.get_doc_content_base(dok_id=self.dok_id)
        logging.debug(respons)
        # Binære data ble riktig
        assert respons["body"]["Content"] == self.filinnhold

    # def test_endre_dokument
    #     fq = eis.filtered_query(
    #         eis.fq_args(
    #             DataObjectName="Dokumentversjon",
    #             FilterExpression="DokumentbeskrivelseId=" + str(dok_id)
    #         )
    #     )
    #     logging.debug(fq)

    # logging.debug(eis.cancel_checkout_doc(
    #     dok_id=dok_id, versjon=1, variant="P"))
    # logging.debug(eis.checkout(
    #     dok_id=dok_id, versjon=1, variant="P"))
    # upload = eis.checkin(
    #     content=filinnhold, dok_id=dok_id, versjon=1, variant="P")
