# py-eis

[![PyPI - Version](https://img.shields.io/pypi/v/py-eis.svg)](https://pypi.org/project/py-eis)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/py-eis.svg)](https://pypi.org/project/py-eis)

-----

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install pyeis
```

## License

`py-eis` is distributed under the terms of the [GPL-2.0-or-later](https://spdx.org/licenses/GPL-2.0-or-later.html) license.

## Utvikling

```bash
hatch test
hatch fmt  # Sjekk formatering
hatch run types:mypy src  # Sjekk typer
```
